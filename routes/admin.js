var express = require('express');
var router = express.Router();
var config = require('../config.json');
var mysql = require('mysql');

var mysqlDB = require('../models/mysql');

/* GET home page. */
router.get('/', function(req, res, next) {
    if(req.session.email==='admin'){
        res.render('admin/base', { title: 'Express' });
    }else{
        res.render('admin/login', { title: 'Express' });
    }
});


router.post('/logout', function(req, res, next) {
    req.session.email = null;
    req.session.userid= null;

        res.render('admin/login', { title: 'Express' });

});

/**
 * login
 */
router.post('/login', function(req, res, next) {
    // console.log(req.body);
    console.log("came hear");
    console.log(req.session.email);


    mysqlDB.getModel('user').findAll({where: {
        email: req.body.email,
        password: req.body.password
    }}).then(function(result){
        console.log(result[0].dataValues);
        if(result.length>0){
            // console.log(req.body.email);
            console.log(result[0].dataValues.id);
            req.session.email = result[0].dataValues.email;
            req.session.userid = result[0].dataValues.id;
            res.render('admin/base', { title: 'Express' });

        }else{
            console.log(result);
            // res.render('login');
        }
    });

});


router.post('/get_booking', function(req, res, next) {

    console.log(req.body);

    var sql = "SELECT * FROM `payments` ORDER BY `payments`.`id` DESC";
    console.log(sql);
    var con = mysql.createConnection({
        host: "localhost",
        user: config.mysql.username,
        password: config.mysql.password,
        database: config.mysql.database,
    })
    con.connect(function(err) {
        if (err) throw err;
        console.log("Connected!");
        con.query(sql, function (err, result) {
            if (err) throw err;
            console.log(result);
            res.send(result);
        });
    });

});



module.exports = router;
/**
 * Created by tharindu on 3/7/2019.
 */


