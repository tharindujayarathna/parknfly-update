/**
 * This file loads all the twig extentions
 * @type {*}
 */
Twig = require('twig');
formOptionBox = require('./form_optionBox');
tableBodyBox  = require('./table_bodyBox');
getURL = require('./router');
getAirports = require('./getAirports');
// registering extensions
Twig.extendFunction('formOptionBox',formOptionBox);
Twig.extendFunction('tableBodyBox' , tableBodyBox);
Twig.extendFunction('getURL' , getURL);
Twig.extendFunction('getAirports' , getAirports);