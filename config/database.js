var config = require('../config.json');

var mysql = require('mysql');

module.exports = mysql.createConnection({
    host: "http://209.97.128.211",
    user: config.mysql.username,
    password: config.mysql.password,
    database: config.mysql.database,
});